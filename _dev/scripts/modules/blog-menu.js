var sidebar = $('.sidebar'),
    menu = $('.menu'),
    sections = $('.article__item'),
    body = $('body'),
    pageContent = $('.maincontent'),
    windowMargin = 60;

var menuFixedContainer = '<div class="fixed-menu"> \
                            <div class="container"> \
                                <div class="fixed-menu__left"></div> \
                                <div class="fixed-menu__right"></div> \
                            </div> \
                        </div>';

module.exports = {
    stickIt : function(wScroll) {
        var stickyStart = sidebar.offset().top - windowMargin;
        if (wScroll >= stickyStart) {


            if (!$('.fixed-menu').length) {
                sidebar.append(menuFixedContainer);

                var
                    fixedMenu = $('.fixed-menu'),
                    menuContainer = fixedMenu.find('.fixed-menu__left'),
                    menuClone = menu.clone();

                fixedMenu.css('top', windowMargin);
                menuContainer.append(menuClone);
                menu.hide();
            }

        } else {

            $('.fixed-menu').remove();
            menu.show();

        }
    },


    chageActiveOnScroll : function(wScroll) {
        $.each(sections, function() {
            var
                $this = $(this),
                windowMargin = $(window).height() / 2,
                topEdge = $this.offset().top - windowMargin,
                bottomEdge = topEdge + $this.height();

            if (wScroll > topEdge && wScroll < bottomEdge) {
                var
                    index = $this.index();

                $('.menu').each(function() {
                    var $this = $(this);

                    $this.find('.menu__item')
                        .eq(index)
                        .addClass('active')
                        .siblings()
                        .removeClass('active');
                });
            }
       });
    },

    showOnMobile : function() {
        $('.sidebar__trigger').on('click', function(e){
            e.preventDefault();

            body.toggleClass('mobile-sidebar');
        });

        $('body').swipe({
            swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
                if (direction == 'right') {
                    body.addClass('mobile-sidebar');
                }

                if (direction == 'left') {
                    body.removeClass('mobile-sidebar');
                }
            },

            allowPageScroll : 'vertical',
            threshold : 150
        });
    },

    clickToArticle : function() {
        $('body').on('click', '.menu__link', function(e){
            e.preventDefault();

            var
                $this = $(this),
                item = $this.closest('.menu__item'),
                index = item.index(),
                reqSection = sections.eq(index),
                windowMargin = $(window).height() / 3,
                sectionOffset = reqSection.offset().top;

            $('body, html').animate({
                'scrollTop' : sectionOffset - 100
            });
        });
    }
}