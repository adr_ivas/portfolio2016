var md = require('./md.js');

var blurLayer = $('.review__form-blur'),
    reviewsSection = $('.reviews');


module.exports = {
    mountains: function () {
        var parallaxContainer = $('.mauntains__parallax');

        if (!md.detect.mobile()) {
            var imgs = parallaxContainer.find('.mountains__layer-pic');

            imgs.each(function() {
                var
                    $this = $(this),
                    src = $this.data('src');

                $this.attr('src', src);
            });

            parallaxContainer.parallax();
        }
    },

    bg : function (wscroll) {
        var bg = $('.header__bg'),
            strafeAmount = wscroll / 45
            strafe = strafeAmount + '%',
            transformString = 'translate3d(0,' + strafe + ',0)';

        bg.css({
            'transform'         : transformString,
            '-moz-transform'    : transformString,
            '-o-transform'      : transformString,
            '-ms-transform'     : transformString,
            '-webkit-transform' : transformString
        });
    },

    blur : function () {
        var
            sectionOffset = reviewsSection.offset().top,
            formOffset = blurLayer.offset().top,
            backgroundStrafe = sectionOffset - formOffset;

        blurLayer.css({
           'background-position' : 'center ' + backgroundStrafe + 'px'
        });
    }
}