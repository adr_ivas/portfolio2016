module.exports = {
    init: function() {
        var pages = {
            welcome : {
                identBlock : $('.auth')
            }
        }

        $('body').jpreLoader({
            splashID : '#preloader',
            loaderVPos : '50%',
            splashVPos : '50%'
        }, function() {
            $('.auth__block').addClass('auth__block_loaded');
        });
    }
}