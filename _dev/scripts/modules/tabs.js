module.exports = {
    init: function () {
        $('.tabs__link').on('click', function(e){
            e.preventDefault();

            var $this = $(this),
                container = $this.closest('.tabs'),
                tab = $this.closest('.tabs__item'),
                index = tab.index(),
                content = container.find('.tabs__content-wrapper').eq(index);

            tab.addClass('active')
                .siblings()
                .removeClass('active');

            content.addClass('active')
                .siblings()
                .removeClass('active');
        });
    }
}