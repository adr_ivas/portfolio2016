module.exports = {
    detect : new MobileDetect(window.navigator.userAgent),
    
    init: function () {
        if (this.detect.mobile()) {
            $('body').addClass('mobile');
        } else {
            $('body').addClass('desktop');
        }
    }
}