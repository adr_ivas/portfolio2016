module.exports = {
    toDownBtn: function() {
        $('.header__down-btn').on('click', function(e){
            e.preventDefault();

            $('body, html').animate({
                scrollTop : $(window).height()
            }, 600);
        });
    },

    screenMenu: function() {
        var menu = $('.screen-menu'),
            body = $('body'),
            blockedClass = 'blocked';

        var svgElems = {
            firstBar : {
                elem : $('#hamburger .hamburger__first'),
                from : "m 5.0916789,20.818994 53.8166421,0",
                to : "M 12.972944,50.936147 51.027056,12.882035"
            },

            secondBar : {
                elem : $('#hamburger .hamburger__second'),
                from : 1,
                to : 0
            },

            thirdBar : {
                elem : $('#hamburger .hamburger__third'),
                from : 'm 5.0916788,42.95698 53.8166422,0',
                to : 'M 12.972944,12.882035 51.027056,50.936147'
            }
        }

        $('.hamburger').on('click touchstart', function(e){
            e.preventDefault();

            var $this = $(this);

            if (!$this.hasClass('hamburger_closed')) {

                menu.show().addClass('active');
                body.addClass(blockedClass);
                animateSvgIcon(svgElems, true);

                setTimeout(function(){
                    showMenuItems(true);
                }, 500);
            } else {
                menu.fadeOut(function() {
                    $(this).removeClass('active');
                });

                body.removeClass(blockedClass);
                animateSvgIcon(svgElems, false);
                showMenuItems(false);
            }

            $this.toggleClass('hamburger_closed');
        });

        // по esc

        $('body').on('keyup', function(e){
            e.preventDefault();

            if (e.keyCode == 27) {
                console.log('dfsdfds');

                if (menu.hasClass('active')) {
                    $('.hamburger').trigger('click');
                }
            }
        });

        function animateSvgIcon(object, isAnimate) {

            for (var item in object) {
                var $this = object[item];

                $this.elem.attr({
                    'from' : isAnimate ? $this.from : $this.to,
                    'to' : isAnimate ? $this.to : $this.from
                });

                $this.elem[0].beginElement();
            }
        }

        function showMenuItems(show) {
            var
                items = $('.screen-menu__link'),
                delay = 100,
                counter = 0,
                timer;

            function each() {
                var $this = items.eq(counter);

                $this.addClass('active');

                if (typeof timer !== 'undefined') {
                    clearTimeout(timer);
                }

                if ($this.length) {
                   timer = setTimeout(each, delay);
                }

                counter++;
            }

            if (show) {
                each();
            } else {
                items.removeClass('active');
            }
        }

        $('.screen-menu').on('touchmove', function(e){
            e.preventDefault();
        });
    }
}