var circles     = require('./modules/circles.js');
var parallax    = require('./modules/parallax.js');
var slider      = require('./modules/slider.js');
var authBtn     = require('./modules/auth-btn.js');
var triggers    = require('./modules/triggers.js');
var preloader   = require('./modules/preloader.js');
var map         = require('./modules/map.js');
var blogMenu    = require('./modules/blog-menu.js');
var md          = require('./modules/md.js'); // mobile detect
var tabs        = require('./modules/tabs.js');

$(document).ready(function(){

    md.init();

    if (!$('.wrapper_admin').length) {
        preloader.init();
    }

    if ($('.mauntains__parallax').length) {
        parallax.mountains();
    }

    if ($('.works').length) {
        slider.init();
    }

    if ($('#google-map').length) {
        map.init();
    }

    authBtn.init();
    triggers.toDownBtn();
    triggers.screenMenu();

    if ($('.blog').length) {
        blogMenu.showOnMobile();
    }

    blogMenu.clickToArticle();

    if ($('.reviews').length) {
        parallax.blur()
    }

    tabs.init();
}); // -> ready_end;

$(window).scroll(function(){
    var wScroll = $(window).scrollTop();

    parallax.bg(wScroll);

    if ($('.sidebar').length) {
        blogMenu.stickIt(wScroll);
        blogMenu.chageActiveOnScroll(wScroll);
    }

    if ($('.skills').length) {
        circles.init(wScroll);
    }
}); // -> scroll_end;

$(window).resize(function(){
    if ($('.reviews').length) {
        parallax.blur()
    }
}); // -> resize_end;