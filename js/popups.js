$(document).ready(function(){
    var options = {
        maxWidth	: 800,
        maxHeight	: 600,
        fitToView	: true,
        autoSize	: true,
        closeClick	: false,
        openEffect	: 'none',
        closeEffect	: 'none'
    };

    $('.tabs__form-submit').on('click', function(e){
        e.preventDefault();

        $.fancybox.open($('#popup'), options);
    });
}); // -> ready_end;

